from app.model.customer_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

def shows():
    dbresult = mysqldb.showUsers()
    result = []
    for items in dbresult:
        user = {
            "USERID" : item[0],
            "USERNAME" : items[1],
            "NAMADEPAN" : items[2],
            "NAMABELAKANG": items[3],
            "EMAIL": items[4]
        }
        result.append(user)

    return jsonify(result)

def show(**params):
    dbresult = mysqldb.showUserById(**params)
    user = {
            "USERID" : item[0],
            "USERNAME" : items[1],
            "NAMADEPAN" : items[2],
            "NAMABELAKANG": items[3],
            "EMAIL": items[4]
        }

        return jsonify(result)

def insert(**params):
    mysqldb.insertUser(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def update(**params):
    mysqldb.updateUserById(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def delete(**params):
    mysqldb.deleteUserById(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def token(**params):
    dbresult = mysqldb.showUserByEmail(**params)
    if dbresult is not None:
        user = {
            "USERNAME" : dbresult[1],
            "EMAIL" : dbresult[4]
        }
        expires = datetime.timedate(days=1)
        access_token = create_access_token(user, fresh=True, expires_delta=expires)

        data = {
            "data":user,
            "token_access":access_token
        }
    else:
        data = {
            "message":"Email tidak terdaftar"
        }
    return jsonify(data)