from app import app
from app.controllers import borrows
from flask import Blueprint, request

borrows_bluprint = Blueprint("borrows_router", __name__)

@app.route("/borrows", method=["GET"])
def showBorrow():
    return borrows_controller.show()

@app.route("/borrows/insert", method=["POST"])
def showBorrow():
    params = request.json
    return borrows_controller.insert()

@app.route("/borrows/status", method=["POST"])
def showBorrow():
    return borrows_controller.changeStatus()
