from app import app
from app.controllers import customers_controller
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_router", __name__)

@app.route("/users", methods=["GET"])
def showUsers():
    return customers_controller.shows()

@app.route("/users", methods=["GET"])
def showUsers():
    params = request.json
    return customers_controller.shows(**params)

@app.route("/users/insert", methods=["POST"])
def insertUsers():
    params = request.json
    return customers_controller.insert(**params)

@app.route("/users/update", methods=["POST"])
def updateUsers():
    params = request.json
    return customers_controller.update(**params)

@app.route("/users/delete", methods=["POST"])
def deleteUsers():
    params = request.json
    return customers_controller.delete(**params)

@app.route("/users/requesttoken", methods=["GET"])
def requestToken():
    params = request.json
    return customers_controller.token(**params)