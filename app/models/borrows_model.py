from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                            database='perpustakaan',
                            user='root'
                            password='lumbantobing09')
        except Exception as e:
            print(e)

    def showBorrowByEmail(self):
        cursor = self.db.cursor()
        query = '''
        select customers.USERNAME, borrows.* 
        from borrows
        inner join customers on borrows.userid = customers.USERID
        where customer.EMAIL = "{0}" and borrows.isactive = 1;
        '''.format(params["email"])
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertBorrow(self, **params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor = self.db.cursor()
        query = '''
        insert into ({0})
        values {1};
        '''.format(column, values)
        cursor.execute(query)

    def updateBorrow(self, **params):
        borrowid = params["borrowid"]
        cursor = self.db.cursor()
        query = '''
        update borrows
        set isactive = 0
        where borrowid = {0};
        '''.format(borrowid)
        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()